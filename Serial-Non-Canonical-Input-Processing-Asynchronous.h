/*
 * Serial-Non-Canonical-Input-Processing-Asynchronous.h
 *
 *  Created on: Apr 30, 2015
 *      Author: sherpa
 */

#ifndef SERIAL_NON_CANONICAL_INPUT_PROCESSING_ASYNCHRONOUS_H_
#define SERIAL_NON_CANONICAL_INPUT_PROCESSING_ASYNCHRONOUS_H_

#include "zTTLV_Buffer.h"

typedef enum {
	zTTLV_Decode_BufferLength,
	zTTLV_Decode_ItemTag,
	zTTLV_Decode_ItemType,
	zTTLV_Decode_ItemLength,
	zTTLV_Decode_ItemValue,
} zTTLV_Decode_t;

#endif /* SERIAL_NON_CANONICAL_INPUT_PROCESSING_ASYNCHRONOUS_H_ */
